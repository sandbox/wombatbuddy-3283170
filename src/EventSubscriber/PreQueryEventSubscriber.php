<?php

namespace Drupal\altering_solarium_query\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\NodeInterface;
use Drupal\search_api_solr\Event\SearchApiSolrEvents;
use Drupal\search_api_solr\Event\PreQueryEvent;

/**
 * Alter the Solarium query to display only users that are participants.
 */
class PreQueryEventSubscriber implements EventSubscriberInterface {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(RouteMatchInterface $route_match, EntityTypeManagerInterface $entity_type_manager) {
    $this->routeMatch = $route_match;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Alter the Solarium query to display only users that are participants.
   *
   * We get a nid of a node from URL, then we read uids from
   * the "field_participants" and filter a users by these uids.
   *
   * @param \Drupal\search_api_solr\Event\PreQueryEvent $event
   *   The event to process.
   */
  public function onPreQueryEvent(PreQueryEvent $event) {
    if ($this->routeMatch->getRouteName() !== 'view.participants.page_1') {
      return;
    }

    $node = $this->routeMatch->getParameter('node');
    // For some reason a node doesn't load automatically from the route. We get
    // just nid despite we have "%node" in view's path. Therefore, we will do a
    // check and if it does not load, then load it ourselves.
    if (!$node instanceof NodeInterface) {
      $nid = $this->routeMatch->getRawParameter('node');
      $node = $this->entityTypeManager->getStorage('node')->load($nid);
    }

    $uids = array_column($node->field_participants->getValue(), 'target_id');

    $searchApiQuery = $event->getSearchApiQuery();
    $solariumQuery = $event->getSolariumQuery();
    $solrFieldNames = $searchApiQuery
      ->getIndex()
      ->getServerInstance()
      ->getBackend()
      ->getSolrFieldNames($searchApiQuery->getIndex());
    // Get the Solr version of the "uid" field.
    $uidSolrFieldName = $solrFieldNames['uid'];
    // Create the string of user ids for using in the filter.
    $uidsString = implode(' ', $uids);
    $solariumQuery
      ->createFilterQuery('uid_filter')
      ->setQuery($uidSolrFieldName . ':(' . $uidsString . ')');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      SearchApiSolrEvents::PRE_QUERY => ['onPreQueryEvent'],
    ];
  }

}
